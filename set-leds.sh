#!/bin/sh

PATH="/sbin:/usr/sbin:/bin:/usr/bin"

set -e

# In case the Iomega iConnect is starting, e.g. due booting.
do_start() {
    # Show the default power on status by the blue color within the Power LED.
    echo "default-on" >/sys/class/leds/power:blue/trigger

    # And let the red color indicate CPU activities in the Power LED.
    echo "cpu" >/sys/class/leds/power:red/trigger

    # The OTB LED will show activity by remote logins through SSH.
    iptables -A INPUT -p tcp --dport 22   -j LED --led-trigger-id tcpssh --led-delay 100
    ip6tables -A INPUT -p tcp --dport 22   -j LED --led-trigger-id tcpssh --led-delay 100
    echo "netfilter-tcpssh" >/sys/class/leds/otb:blue/trigger

    return 0
}

# If some process is requesting a shutdown of the hardware switch the LED
# modes to reflect a non default status change.
do_stop() {
    # Let the blue Power LED blinking.
    echo "timer" >/sys/class/leds/power:blue/trigger

    # Turning the red Power LED into permanent red light mode.
    echo "default-on" >/sys/class/leds/power:red/trigger

    # Turning of the OTB LED.
    echo "none" >/sys/class/leds/otb:blue/trigger

    # Turning of all possible high states on the LEDs for the USB ports.
    echo "0" >/sys/class/leds/usb1:blue/brightness
    echo "0" >/sys/class/leds/usb2:blue/brightness
    echo "0" >/sys/class/leds/usb3:blue/brightness
    echo "0" >/sys/class/leds/usb4:blue/brightness

    return 0
}

case "$1" in
    start)
        do_start
        ;;
    restart|reload|force-reload)
        echo "Error: argument '$1' not supported" >&2
        exit 3
        ;;
    stop)
        do_stop
        ;;
    *)
        echo "Usage: $0 start|stop" >&2
        exit 3
        ;;
esac
