#!/bin/sh

# This file is part of the package iconnect-tools.
# It's called by udev rule
#
#    /lib/udev/rules.d/99-usb-leds.rules

LED=`echo "$DEVPATH" | sed -r 's/.*\/\w-\w\.(\w)\/.*/\1/'`

if [ "${LED}" != '' ]; then
    if [ "${ACTION}" = "add" ]; then
        echo "1" >/sys/class/leds/usb"${LED}":blue/brightness
    else
        echo "0" >/sys/class/leds/usb"${LED}":blue/brightness
    fi
else
    echo "Uups! "$0": \$DEVPATH is empty! Skript not called by udev?"
    exit 1
fi

exit 0
